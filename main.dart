import 'package:flutter/material.dart';
import 'package:scartp/screen/CheckoutScreen.dart';
import 'package:scartp/screen/categories.dart';
import 'package:scartp/screen/home.dart';
import 'package:scartp/screen/profile.dart';
import 'package:scartp/screen/search.dart';
import 'model/ProductModel.dart';
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
 static final String title = 'User Profile';
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.blue.shade300,
       dividerColor: Colors.black,
      ),
      title: 'Mobtech',
      home:MyHomePage(),
      routes: {
      'Categories' : (context){
        return Categories();
      }
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with SingleTickerProviderStateMixin {
   List<ProductModel> cart = [];
  int sum = 0;
 TabController _tabController;

void initState(){
  super.initState();
  _tabController = TabController(length: 4, vsync: this);
}

void dispose(){
  super.dispose();
  _tabController.dispose();
}
  @override
  Widget build(BuildContext context) {
   
    return Scaffold(
      body: TabBarView(
      children: <Widget>[
        Home(),
        Search(),
        ProfilePage(),
        CheckoutScreen(cart, sum)
        ],
          controller: _tabController,
      ),
       bottomNavigationBar: Container(
       padding: EdgeInsets.all(16.0),
            child: ClipRRect(
         borderRadius: BorderRadius.all(Radius.circular(50.0),
         ),
              child: Container(
           color: Colors.black54,
                child: TabBar(
             labelColor: Colors.black,
             unselectedLabelColor: Colors.white,
             labelStyle: TextStyle(fontSize:10.0),
             indicator: UnderlineTabIndicator(borderSide: BorderSide(color: Colors.black54,width: 0.0),
             insets:EdgeInsets.fromLTRB(50, 0, 50, 0),
             
             ),
             indicatorColor:Colors.black54,
             tabs: <Widget>[
            Tab(
              icon: Icon(Icons.home, size: 24,),
              child: Text('Home'),
            ),
            Tab(
               icon: Icon(Icons.search,size: 24,),
              child: Text('Search'),
            ),
            Tab(
               icon: Icon(Icons.person_outline_outlined,size: 24,),
              child: Text('profile'),
            ),
             Tab(
               icon: Icon(Icons.add_shopping_cart,size: 24,),
              child: Text('cart'),
            ),
           ],
           controller: _tabController,
           ),
         ),
       ),
     ),
    );
  }
}