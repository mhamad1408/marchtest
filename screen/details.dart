import 'package:flutter/material.dart';
class Details extends StatefulWidget {
  @override
  _DetailsState createState() => _DetailsState();
}

class _DetailsState extends State<Details> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
      title: Text("Details"),
      centerTitle: true,
      ),
      body: ListView(
      children: <Widget>[
      Container(
      height: 300,
      child: GridTile(
      child:Image.asset('assets/images/w17.jpg'),
      footer: Container(
      height: 80,
      color:Colors.black.withOpacity(0.3),
      child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
             child: Container(
              padding: EdgeInsets.all(10),
              child: Text('P30 Pro',style: TextStyle(color: Colors.white, fontSize: 20, fontWeight:FontWeight.w700),
              ),
            ),
          ),
           Padding(
             padding: const EdgeInsets.all(10),
             child: Text('300\$',style: TextStyle(color: Colors.white, fontSize: 20, fontWeight:FontWeight.w700),
          ),
           ),
        ],
      ),
      ),
      ),
      ),
      //End Header Page
      Container(
      padding: EdgeInsets.all(10),
      child: Text('Description',
      style: TextStyle(fontSize:20),
      ),
      ),
      Container(
      padding: EdgeInsets.all(10),
      child: Column(
      children: <Widget>[
      Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.all(10),
        color: Colors.white,
        child: RichText(
        text: TextSpan(
        style: TextStyle(
          fontSize: 18,
         ),
        children: <TextSpan>[
          TextSpan(text:'Model:',
         ),
         TextSpan(text:'P30 Pro',
         ),
        ],
        ),
        ),
      ),
      Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.all(10),
        color: Colors.white,
        child:RichText(
        text: TextSpan(
        children: <TextSpan>[
          TextSpan(text:'Screen:',style: TextStyle(
          fontSize: 18,
         ),
         ),
         TextSpan(text:'Supper Amoled 5.5',style: TextStyle(
          fontSize: 18,
         ),
         ),
        ],
        ),
        ),
      ),
       Container(
       margin: EdgeInsets.only(top: 5),
       child: Expanded(child: IconButton(icon: Icon(Icons.add), onPressed: (){
        }),
       ),
     ),
      ],
      ),
      ),
    ],
    ),
  );
  }
}